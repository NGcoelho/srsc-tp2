package api;

import com.sun.deploy.security.UserDeclinedException;
import exceptions.ResourceNotSpecifiedException;
import exceptions.UserLoggedInException;
import exceptions.WrongCredentialsException;


import javax.net.ServerSocketFactory;
import javax.net.ssl.*;
import java.io.*;
import java.net.ServerSocket;
import java.nio.charset.StandardCharsets;
import java.nio.file.AccessDeniedException;
import java.security.KeyStore;
import java.security.MessageDigest;



public class FileServerAPI extends ClassServer{

    static String storage_host = null, access_host = null, auth_host = null;
    static String[] argu;
    String logged_user;
    static int storage_port = -1,access_port = -1,server_port=-1,auth_port = -1;
    private static SSLSocket storage_socket, access_socket, auth_socket;
    private static ServerSocket ss;


    /**
     * This is the File Server Client API. Because it's responsible of handling client requests and redirect them into the right modules ( Auth, Access, Storage ).
     * It acts as a Server for the Client  and as a client to the modules.
     * **/

    /**
     * TODO: REVISION: replace startHandshake for getSession for complete handshakes (DONE)
     * TODO: REVISION: maybe upon login the user could start already in it's directory
     * TODO: REVISION: API could use a shell to send shutdown signals to the shell (DONE)
     */

    /**
     * Constructs a ClassServer based on <b>ss</b> and
     * obtains a file's bytecodes using the method <b>getBytes</b>.
     *
     * @param ss
     */
    protected FileServerAPI(ServerSocket ss) {
        super(ss);
        logged_user = null;
    }

    //Starting point
    public static void main(String[] args) throws Exception {
        argu=args;
        if(args.length < 7){
            System.out.println(
                    "USAGE: java FileServerAPI " +
                            "server_port auth_host auth_port access_host access_port storage_host storage_port");
            System.exit(-1);
        }
        try{
            server_port = Integer.parseInt(args[0]);
            auth_host = args[1];
            auth_port = Integer.parseInt(args[2]);
            access_host = args[3];
            access_port = Integer.parseInt(args[4]);
            storage_host = args[5];
            storage_port = Integer.parseInt(args[6]);
            start_server("TLS",server_port);


        }catch(IllegalArgumentException e){
            System.out.println(
                    "USAGE: java SSLSocketClientWithClientAuth " +
                            "host port command path");
            System.exit(-1);
        }
        try{
            jump_start_socket("storage");
            jump_start_socket("access");
            jump_start_socket("auth");
            start_root_shell();

        }catch(Exception e){
            e.printStackTrace();;
        }
    }

    private static void shutdown_module(String module) throws Exception {
        PrintWriter out;

        switch (module){
            case "access":
                out = new PrintWriter(
                        new BufferedWriter(
                                new OutputStreamWriter(
                                        access_socket.getOutputStream())));
                break;
            case "auth":
                 out = new PrintWriter(
                        new BufferedWriter(
                                new OutputStreamWriter(
                                        auth_socket.getOutputStream())));
                break;
            case "storage":
                out = new PrintWriter(
                        new BufferedWriter(
                                new OutputStreamWriter(
                                        storage_socket.getOutputStream())));

                break;
                default:
                    throw new Exception("module "+module+" not recognized");
        }

        out.println("GET shutdown HTTP/1.1");
        out.println();
        out.flush();

        BufferedReader response;

        switch (module){
            case "access":
                 response = new BufferedReader(
                        new InputStreamReader(
                                access_socket.getInputStream()));
                break;
            case "auth":
                response = new BufferedReader(
                        new InputStreamReader(
                                auth_socket.getInputStream()));
                break;
            case "storage":
                response = new BufferedReader(
                        new InputStreamReader(
                                storage_socket.getInputStream()));

                break;
            default:
                throw new Exception("module "+module+" not recognized");
        }
        String success = "failed!";
        String inputLine;
        while ((inputLine = response.readLine()) != null)
            if (inputLine.contains("200 OK"))
                success = "done with success!";
        if(success.equals("done with success!")){
            switch (module){
                case "access":
                    access_socket.close();
                    break;
                case "auth":
                    auth_socket.close();
                    break;
                case "storage":
                    storage_socket.close();
                    break;
                default:
                    throw new Exception("module "+module+" not recognized");
            }
        }

        System.out.println(module+" shutdown "+success);


    }


    private static void start_root_shell() throws Exception {
        boolean allowed = false;
        BufferedReader input = new BufferedReader(new InputStreamReader(System.in));
        while (!allowed) {
            Console console = System.console();

            System.out.print("root password:");
            String password = console != null ? console.readPassword().toString() : input.readLine();
            MessageDigest digest = MessageDigest.getInstance("SHA-256");
            byte[] encodedhash = digest.digest(
                    password.getBytes(StandardCharsets.UTF_8));
            String msg = "root " + bytesToHex(encodedhash);
            PrintWriter out = new PrintWriter(
                    new BufferedWriter(
                            new OutputStreamWriter(
                                    auth_socket.getOutputStream())));
            out.println("GET " + msg + "  HTTP/1.1");
            out.println();
            out.flush();
            BufferedReader response = new BufferedReader(
                    new InputStreamReader(
                            auth_socket.getInputStream()));

            String inputLine;
            while ((inputLine = response.readLine()) != null) {
                if (inputLine.contains("200 OK")) {
                    allowed = true;

                }
                System.out.println(inputLine);

            }
            System.out.println();
            response.close();
        }
        jump_start_socket("auth");
        System.out.println("Welcome to Curtes? Filesystem root shell!");

        input = new BufferedReader(new InputStreamReader(System.in));
        String line, working_directory="/";
        System.out.print("root:"+working_directory);
        PrintWriter out;
        String[] split_line;

        while(!wants_off(line = input.readLine())){
             split_line = line.split(" ");
            if(split_line[0].equals("write")){
                System.out.println("\n You may write the file. Write EOF to finish.");
                System.out.println("The file will only be written if it exists and you have access.\n");
                BufferedReader input_reader = new BufferedReader(new InputStreamReader(System.in));
                ByteArrayOutputStream file_writer = new ByteArrayOutputStream();
                while(true) {
                    String line1 = input_reader.readLine();
                    if (line1.equals("EOF")){
                        file_writer.close();
                        break;
                    }
                    file_writer.write((line1+"/n").getBytes());
                }
                out = new PrintWriter(
                        new BufferedWriter(
                                new OutputStreamWriter(storage_socket.getOutputStream())));
                out.println("GET "+line+" "+file_writer.toString()+"\nHTTP/1.1");
                out.println();
                out.flush();

                if (out.checkError())
                    System.out.println(
                            "SSLSocketClient: java.io.PrintWriter error");
            }
            else if(split_line[0].equals("shutdown")){

                if(split_line.length == 2 && split_line[1].equals("all")){
                    shutdown_module("auth");
                    shutdown_module("access");
                    shutdown_module("storage");
                }
                else {
                    if(split_line.length != 1)
                        for (int i = 1; i < split_line.length; i++) {
                            try{
                                shutdown_module(split_line[i]);
                            }catch(Exception e){
                                if(e.getMessage().equals("module not recognized"))
                                    System.err.println("usage: shutdown [all] [access||auth||storage]");
                                break;
                        }
                    }
                    else
                        System.err.println("usage: shutdown [all] [access||auth||storage]");

                }
                System.out.print("\nroot:"+working_directory);
                continue;
            }
            else {
                out = new PrintWriter(
                        new BufferedWriter(
                                new OutputStreamWriter(
                                        storage_socket.getOutputStream())));
                out.println("GET " + line + "  HTTP/1.1");
                out.println();
                out.flush();
                /*
                 * Make sure there were no surprises
                 */
                if (out.checkError())
                    System.out.println(
                            "SSLSocketClient: java.io.PrintWriter error");
            }

            /* read response */
            BufferedReader response = new BufferedReader(
                    new InputStreamReader(
                           storage_socket.getInputStream()));

            String inputLine;
            String command[] = line.split(" ");

            while ((inputLine = response.readLine()) != null){

                if(!inputLine.contains("200 OK") && !inputLine.contains("Content-Length") && !inputLine.contains("Content-Type")  )
                    if(command[0].equals("cd") && !inputLine.contains("400") && !inputLine.contains("HTTP/1."))
                        working_directory = inputLine;
                    else System.out.println(inputLine);

            }
            response.close();
            jump_start_socket("storage");
            System.out.print("\nroot:"+working_directory);
        }
        System.out.println("Shutting down any remaining modules...");
        if(!storage_socket.isClosed())
            shutdown_module("storage");
        if(!auth_socket.isClosed())
            shutdown_module("auth");
        if(!access_socket.isClosed())
            shutdown_module("access");
        System.out.println("Have a good day.");
        System.exit(0);
    }

    private static boolean wants_off(String line){
        return line.equals("quit") || line.equals("logout");
    }


    private static String bytesToHex(byte[] hash) {
        StringBuffer hexString = new StringBuffer();
        for (int i = 0; i < hash.length; i++) {
            String hex = Integer.toHexString(0xff & hash[i]);
            if(hex.length() == 1) hexString.append('0');
            hexString.append(hex);
        }
        return hexString.toString();
    }

    private static void jump_start_socket(String socket) throws IOException {
        switch (socket){
            case "storage":
                storage_socket = create_client_socket("storage",storage_host,storage_port);
                storage_socket.getSession();
                break;
            case "access":
                access_socket = create_client_socket("access",access_host,access_port);
                access_socket.getSession();
                break;
            case "auth":
                auth_socket = create_client_socket("auth", auth_host, auth_port);
                auth_socket.getSession();
                break;
        }
    }

    //Auxiliary method used to create the sockets to communicate with the modules
    private static SSLSocket create_client_socket(String who,String host, int port) throws IOException {

            SSLSocketFactory factory = null;
            try{
                SSLContext ctx;
                KeyManagerFactory kmf;
                KeyStore ks;
                char[] passphrase = "myclient".toCharArray();
                ctx = SSLContext.getInstance("TLS");
                kmf = KeyManagerFactory.getInstance("SunX509");
                ks = KeyStore.getInstance("JKS");
                switch (who){
                    case "storage":
                        ks.load(new FileInputStream("APIFSkeystore"),passphrase);
                        break;
                    case "access":
                        passphrase = "123456".toCharArray();
                        ks.load(new FileInputStream("APIACkeystore"),passphrase);
                        break;
                    case "auth":
                        passphrase = "123456".toCharArray();
                        ks.load(new FileInputStream("authkeystore"),passphrase);
                        break;
                        default:
                            throw new Exception("Unrecognized entity");
                }
                kmf.init(ks,passphrase);
                ctx.init(kmf.getKeyManagers(),null,null);
                factory = ctx.getSocketFactory();

            }
            catch(Exception e){
                throw new IOException(e.getMessage());

            }
            return (SSLSocket)factory.createSocket(host,port);
    }



    /**
     * The getBytes() method is abstract and inherited from Class Server. It is the method that is executed upon the server receiving content.
     * Because this is the API, all of the possible requests made by the client are received here, and must be handled accordingly.
     * This includes normal shell scenario as well as authentication scenario
     * **/
    @Override
    public byte[] getBytes(String path) throws Exception {

        ByteArrayOutputStream byte_out = new ByteArrayOutputStream();
        if(path.equals("quit") || path.equals("logout")){
            logged_user = null;
            byte_out.write("/".getBytes());
            send_command_to_storage("cd",new ByteArrayOutputStream());
           // start_server("TLS",server_port);
        }
        else if(path.contains("shutdown")){
            throw new AccessDeniedException("clients are not allowed to shut down modules.");
        }
        else{
                String[] split_path = path.split(" ");
                String full_path = path;
                StringBuilder builder = new StringBuilder();
                // ls
                // ls user3/abc/
                //case command only : we are working directory


                if(split_path[0].equals("login")){
                    if(logged_user != null) {
                        jump_start_socket("auth");
                        throw new UserLoggedInException(logged_user + " is already logged in.");
                    }


                    String user, msg;
                    user = split_path[1];

                    if(user.equals("root")) {
                        jump_start_socket("auth");
                        throw new UserDeclinedException("root access is not allowed through this shell.");
                    }

                    msg = user + " ";
                    for (int i = 2; i < split_path.length; i++) {
                        if(i!=2) msg+=" "+split_path[i];
                        else msg+=split_path[i];
                    }
                    PrintWriter out = new PrintWriter(
                            new BufferedWriter(
                                    new OutputStreamWriter(auth_socket.getOutputStream())));

                    out.println("GET "+ msg +" HTTP/1.1");
                    out.println();
                    out.flush();

                    if(out.checkError())
                        System.out.println(
                                "SSLSocketClient: java.io.PrintWriter error");

                    BufferedReader in = new BufferedReader(
                            new InputStreamReader(auth_socket.getInputStream()));


                    String response = in.readLine();

                    if(response.contains("200")){
                        logged_user = user;

                        jump_start_socket("auth");
                        byte[] receive = send_command_to_storage("cd "+logged_user, new ByteArrayOutputStream()).toByteArray();
                        jump_start_socket("storage");
                        return receive;

                    }else if(response.contains("400")){
                        jump_start_socket("auth");
                        throw new WrongCredentialsException("Login incorrect");
                    }

                }
                else if(split_path.length == 1){
                    ByteArrayOutputStream out1 = send_command_to_storage("pwd",byte_out);
                    String[] full_response = out1.toString().split("\n");
                    builder.append(split_path[0] + " " +full_response[full_response.length-1]);
                    jump_start_socket("storage");
                    full_path = builder.toString();
                    check_access_for_path(full_path);

                }
                else {
                    if(split_path[0].equals("write")){
                        if(!split_path[1].startsWith("/")) {
                            ByteArrayOutputStream out1 = send_command_to_storage("pwd", byte_out);
                            String[] full_response = out1.toString().split("\n");
                            String pre_path = full_response[full_response.length - 1];
                            builder.append(split_path[0] + " "
                                    + pre_path
                                    + (pre_path.contains("/") && pre_path.lastIndexOf("/") == pre_path.length() - 1 ? "" : "/") + split_path[1]);
                            jump_start_socket("storage");
                            full_path = builder.toString();
                            check_access_for_path(full_path);
                        }
                        else check_access_for_path(split_path[1]);

                    }
                    else{
                        for (int i = 1; i < split_path.length ; i++) {
                            //path does not have / so it's working directory
                            if(!split_path[i].startsWith("/")) {
                                ByteArrayOutputStream out1 = send_command_to_storage("pwd",byte_out);
                                String[] full_response = out1.toString().split("\n");
                                String pre_path = full_response[full_response.length-1];
                                builder.append(split_path[0] + " "
                                        +pre_path
                                        +(pre_path.contains("/") && pre_path.lastIndexOf("/") == pre_path.length()-1 ? "" : "/")+split_path[i]);
                                jump_start_socket("storage");
                                full_path = builder.toString();
                                check_access_for_path(full_path);

                                builder = new StringBuilder();
                            }
                            else check_access_for_path(full_path);
                        }
                    }
                }
            byte_out.reset();
            byte_out = send_command_to_storage(path,byte_out);
        }
        jump_start_socket("storage");
        return byte_out.toByteArray();
    }
    /**
     * Method used to send commands to the access module , and read the response
     * **/
    private void check_access_for_path(String full_path) throws IOException {

        PrintWriter out = new PrintWriter(
                new BufferedWriter(
                        new OutputStreamWriter(access_socket.getOutputStream())));

        out.println("GET "+ full_path +" " +logged_user+ " HTTP/1.1");
        out.println();
        out.flush();
        if(out.checkError())
            System.out.println(
                    "SSLSocketClient: java.io.PrintWriter error");

        //Read the response
        BufferedReader in = new BufferedReader(
                new InputStreamReader(access_socket.getInputStream()));

        String input_line;
        ByteArrayOutputStream out1 = new ByteArrayOutputStream();
        while((input_line = in.readLine()) != null) {
            out1.write((input_line+"\n").getBytes());
        }
        String[] full_response = out1.toString().split("\n");
        if(!full_response[full_response.length-1].equals("true")) {
            jump_start_socket("access");
            throw new AccessDeniedException("impossible to access " + full_path + ": access denied.");
        }
        in.close();
        out.close();
        jump_start_socket("access");
    }

    /**
     * Method used to send commands to the storage module , and read the response
     * **/
    private ByteArrayOutputStream send_command_to_storage(String path, ByteArrayOutputStream byte_out) throws IOException {
        PrintWriter out = new PrintWriter(
                new BufferedWriter(
                        new OutputStreamWriter(storage_socket.getOutputStream())));

        out.println("GET "+ path +" HTTP/1.1");
        out.println();
        out.flush();
        if(out.checkError())
            System.out.println(
                    "SSLSocketClient: java.io.PrintWriter error");

        //Read the response
        BufferedReader in = new BufferedReader(
                new InputStreamReader(storage_socket.getInputStream()));

        String input_line;
        while((input_line = in.readLine()) != null)
            byte_out.write((input_line+"\n").getBytes());
        in.close();
        out.close();
        return  byte_out;
    }

    //This method will start the server. The socket is created and thrown into the super constructor.
    //The super class extends a Runnable interface so a thread will be created.
    private static void start_server(String type, int port) throws ResourceNotSpecifiedException {
        try {
            // Create a Socket Factory of type mentioned
            // Plain Socket ... or SSL Socket and creates
            // the server socket in the specified port
            ServerSocketFactory ssf = getServerSocketFactory(type);
            ss = ssf.createServerSocket(port);
            ssf.createServerSocket();
            ((SSLServerSocket) ss).setEnabledProtocols(new String[]{"TLSv1.2"});
            ((SSLServerSocket) ss).setEnabledCipherSuites(new String[]{"TLS_RSA_WITH_AES_128_GCM_SHA256"});
            ((SSLServerSocket) ss).setNeedClientAuth(true);



            new FileServerAPI(ss);

        } catch (IOException e) {
            System.out.println("Problem with sockets: unable to start ClassServer: " +
                    e.getMessage());
            e.printStackTrace();

        }
    }
    //Auxiliary method used to provide the factory in charge of creating the socket.
    private static ServerSocketFactory getServerSocketFactory(String type) {
        if (type.equals("TLS")) {
            SSLServerSocketFactory ssf = null;
            try {
                // set up key manager to do server authentication
                SSLContext ctx;
                KeyManagerFactory kmf;
                KeyStore ks;

                // Depending on the passphrase used to protect the
                // FSserverkeystore used. Remember, this is the keystore
                // where the server stores the keys.

                //char[] passphrase = "passphrase".toCharArray();
                 ;//

                ctx = SSLContext.getInstance("TLS");
                kmf = KeyManagerFactory.getInstance("SunX509");
                ks = KeyStore.getInstance("JKS");

//              keystore - chaves do servidor: criada com passwd indicada

                ks.load(new FileInputStream("APIClientkeystore"), "123456".toCharArray());//
                kmf.init(ks, "123456".toCharArray());
                ctx.init(kmf.getKeyManagers(), null, null);

                // only to deal with the keystore internal representation
                // you have KeyStore class with a lot of methods - see
                // the Java documentation.
                // Ex., KeyStore.getKey() takes a string corresponding to
                // an alias name and a char array representing a password
                // protecting the entry of the keystore, returning
                // java.security.Key object


                ssf = ctx.getServerSocketFactory();
                return ssf;
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            return ServerSocketFactory.getDefault();
        }
        return null;
    }
}
