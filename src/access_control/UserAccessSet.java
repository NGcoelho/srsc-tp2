package access_control;

import java.util.HashMap;

public class UserAccessSet {

    private String username;

    private HashMap<String, Access> user_accesses;

    public UserAccessSet(String username){
        this.username = username;
        this.user_accesses = new HashMap<>();
    }

    public void add_access(String path, Access access){
        this.user_accesses.putIfAbsent(path.trim(),access);
    }

    public Access get_access(String path){
        //MAY BE A RECURSIVE PATH
        boolean is_path_direct = user_accesses.containsKey(path);
        if(is_path_direct)
            return user_accesses.get(path);
        else{
            //PATH IS RECURISVE, WE HAVE TO SPLIT IT TO SEE IF ANY OF THE PRECEDING DIRECTORIES ARE ALLOWED
            String[] split_path = path.split("/");
            String path_building = "";
            for (int i = 0; i < split_path.length ; i++) {
                if(split_path[i].equals(""))
                    path_building += "/";
                else path_building += split_path[i]+"/";

                if(user_accesses.containsKey(path_building))
                    return user_accesses.get(path_building);
            }

        }
        return null;
    }

    public HashMap<String,Access> get_all_accesses(){
        return user_accesses;
    }
}