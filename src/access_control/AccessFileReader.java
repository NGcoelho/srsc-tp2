package access_control;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;

public class AccessFileReader {

    private static final String access_file_name = "example_access_controls.txt";

    public static void main(String[] args) throws IOException {
        HashMap<String,UserAccessSet> x = readAccessesFile();
    }



    public static HashMap<String, UserAccessSet> readAccessesFile() throws IOException {
        HashMap<String, UserAccessSet> user_accesses = new HashMap<>();
        File access_file = new File(access_file_name);
        Scanner scanner = new Scanner(access_file);
        List<String> userList = new LinkedList<>();

        while(scanner.hasNextLine()){
            String next = scanner.nextLine().trim();
            if(next.contains("<") && !next.contains("</")) {
                while (!isAccessesEnd(next) && scanner.hasNextLine()) {
                    userList.add(next);
                    next = scanner.nextLine().trim();
                }
            }
            if(isAccessesEnd(next)){
                user_accesses.put(userList.get(0).replace("<","").replace(">", "").trim(),create_accesses_user( userList));
                userList = new LinkedList<>();
            }
        }


        return user_accesses;
    }

    private static UserAccessSet create_accesses_user(List<String> userList) {
        //list[0] = name
        String name ="";
        UserAccessSet user_access_set = null;
        for (int i = 0; i < userList.size(); i++) {
            String line = userList.get(i);
            if(i==0) {
                name = line.replace("<", "").replace(">", "");
                user_access_set = new UserAccessSet(name);
            }
            if(isAccessStart(line)){
                //ACCESS START HERE: MAY BE W, R OR RW
                String access_type = line.replace("<","").replace(">","");
                i++;
                line = userList.get(i);
                while(!isAccessEnd(line)){
                    String resource = null;
                    boolean fileOrDirectory = false;
                    if(line.contains("<path>")) {
                        resource = line.replace("<path>", "").replace("</path>", "").trim();
                        fileOrDirectory = false;
                    }
                    else if(line.contains("<file>")) {
                        resource = line.replace("<file>", "").replace("</file>", "").trim();
                        fileOrDirectory = true;
                    }
                    if(access_type.equals("r")){
                        user_access_set.add_access(resource,new Access(resource,fileOrDirectory,false,true));
                    }
                    else if(access_type.equals("w")){
                        user_access_set.add_access(resource,new Access(resource,fileOrDirectory,true,false));
                    }
                    else if(access_type.equals("rw")){
                        user_access_set.add_access(resource,new Access(resource,fileOrDirectory,true,true));
                    }

                    i++;
                    line = userList.get(i);
                }
            }
        }
        return user_access_set;
    }

   private static boolean isAccessStart(String line){
        return line.equals("<w>") || line.equals("<r>") || line.equals("<rw>");
   }

    private static boolean isAccessEnd(String line){
        return line.equals("</w>") || line.equals("</r>") || line.equals("</rw>");
    }

    private static boolean isAccessesEnd(String line){
        if(!line.contains("</") || line.contains("<file>") || line.contains("<path>"))
            return false;
        if(line.equals("</r>") || line.equals("</w>") || line.equals("</rw>"))
            return false;
        return true;
    }


}
