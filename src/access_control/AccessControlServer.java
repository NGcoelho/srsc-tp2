package access_control;

import api.ClassServer;
import storage.FileServerStorage;

import javax.net.ServerSocketFactory;
import javax.net.ssl.KeyManagerFactory;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLServerSocket;
import javax.net.ssl.SSLServerSocketFactory;
import java.io.FileInputStream;
import java.io.IOException;
import java.net.ServerSocket;
import java.security.KeyStore;
import java.util.HashMap;

public class AccessControlServer extends ClassServer {

    //BASICAMENTE QUANDO RECEBERMOS UM PEDIDO, TEMOS DE VER SE O USER TEM ACESSO PARA O PEDIDO

        /**
         * This is the Access Control Server. Upon start, it loads the set of accesses of each user
         * from the respective txt file, fits them into the needed data structures and awaits for a request.
         * Upon request, it matches the received path and the user with the info available and grants request if
         * the user has access to said path.
         *
         * **/



    static int port;
    static HashMap<String,UserAccessSet>  user_accesses;

    /**
     * Constructs a ClassServer based on <b>ss</b> and
     * obtains a file's bytecodes using the method <b>getBytes</b>.
     *
     * @param ss
     */
    protected AccessControlServer(ServerSocket ss) {
        super(ss);
    }


    //Starting point
    public static void main(String[] args) throws IOException {
         user_accesses = AccessFileReader.readAccessesFile();
        if(args.length < 2){
            System.out.println(
                    "USAGE: java [-Djavax.net.ssl.trustStore=FSservertruststore] FileServerStorage port [TLS [true]]");
            System.out.println("");
        }
        else{

            if(args.length >= 1){
                port = Integer.parseInt(args[0]);
            }
            String type = "PlainSocket";
            if(args.length >= 2){
                type=args[1];
            }
            //server
            start_server(type,port,args);


        }
        //System.out.println(check_access_for_command("ls /user1/abcdef/", "user1"));

    }
    //This method will start the server. The socket is created and thrown into the super constructor.
    //The super class extends a Runnable interface so a thread will be created.
    private static void start_server(String type, int port, String[] args) {
        try{
            // Create a Socket Factory of type mentioned
            // Plain Socket ... or SSL Socket and creates
            // the server socket in the specified port
            ServerSocketFactory ssf = getServerSocketFactory(type);
            ServerSocket ss = ssf.createServerSocket(port);
            ((SSLServerSocket)ss).setEnabledProtocols(new String[] { "TLSv1.2"});
            ((SSLServerSocket)ss).setEnabledCipherSuites(new String[] {"TLS_RSA_WITH_AES_128_GCM_SHA256"});
            ((SSLServerSocket)ss).setNeedClientAuth(true);

            new AccessControlServer(ss);
        }
        catch(IOException e){
            System.out.println("Problem with sockets: unable to start ClassServer: " +
                    e.getMessage());
            e.printStackTrace();

        }
    }
    //Main method for checking access for a user, of a given command ( command has the path )
    private static boolean check_access_for_command(String command, String user){
        // "ls /user2/abcde/" "user1"
        String[] split_command = command.split(" ");
        String access_type = check_reqs_for_command(split_command[0]);
        String path = split_command[1];
        //Bring out user access set
        UserAccessSet accessSet = user_accesses.get(user);
        Access access = accessSet.get_access(path);
        if(access == null) return false;
        if(access_type.equals("rw"))
            return access.isReadable() && access.isWritable();
        if(access_type.equals("r"))
            return access.isReadable();
        if(access_type.equals("w"))
            return access.isWritable();
        return false;
    }
    //Auxiliary method to verify the access need of a command ( ex : ls = read permission )
    private static String check_reqs_for_command(String command){
        String access_type = "";

        switch (command){
            case "ls" :
                access_type = "r";
                break;
            case "cd":
                access_type = "r";
                break;
            case "read" :
                access_type = "r";
                break;
            case "file" :
                access_type = "r";
                break;
            case "mkdir" :
                access_type = "w";
                break;
            case "touch" :
                access_type = "w";
                break;
            case "write" :
                access_type = "w";
                break;
            case "rm" :
                access_type = "w";
                break;
            case "cp" :
                access_type = "r(sources) + w (destination)";
                break;

        }
        return access_type;

    }
    /**
     * The getBytes() method is abstract and inherited from Class Server. It is the method that is executed upon the server receiving content.
     * In this implementation, the path contains the issued command, a path and the user requesting access. After splitting the string accordingly,
     * access is verified for the given user for the stated path.
     * **/
    @Override
    public byte[] getBytes(String path) throws Exception {
        //path será "ls /user3/abdef/ user1"
        String[] split_path = path.split(" ");
        if(path.equals("shutdown"))
            return "shutdown".getBytes();
        String path_ ="", command = "", user = "";
        command=split_path[0];
        if(split_path.length==2){
            user = split_path[1];
        }
        else {
            path_ = split_path[1];
            user = split_path[split_path.length-1];
        }

        command = command + " " +path_;

        boolean has_access = check_access_for_command(command,user);
        return has_access ? "true".getBytes() : "false".getBytes();
    }

    //Auxiliary method used to provide the factory in charge of creating the socket.
    public static ServerSocketFactory getServerSocketFactory(String type) {
        if (type.equals("TLS")) {
            SSLServerSocketFactory ssf = null;
            try {
                // set up key manager to do server authentication
                SSLContext ctx;
                KeyManagerFactory kmf;
                KeyStore ks;

                // Depending on the passphrase used to protect the
                // FSserverkeystore used. Remember, this is the keystore
                // where the server stores the keys.

                //char[] passphrase = "passphrase".toCharArray();
                char[] passphrase = "123456".toCharArray();//

                ctx = SSLContext.getInstance("TLS");
                kmf = KeyManagerFactory.getInstance("SunX509");
                ks = KeyStore.getInstance("JKS");

//              keystore - chaves do servidor: criada com passwd indicada

                ks.load(new FileInputStream("accesskeystore"), passphrase);//
                kmf.init(ks, passphrase);
                ctx.init(kmf.getKeyManagers(), null, null);

                // only to deal with the keystore internal representation
                // you have KeyStore class with a lot of methods - see
                // the Java documentation.
                // Ex., KeyStore.getKey() takes a string corresponding to
                // an alias name and a char array representing a password
                // protecting the entry of the keystore, returning
                // java.security.Key object


                ssf = ctx.getServerSocketFactory();
                return ssf;
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            return ServerSocketFactory.getDefault();
        }
        return null;
    }
}
