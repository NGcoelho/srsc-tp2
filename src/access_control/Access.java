package access_control;

public class Access {
    private String path;
    private boolean isFile, writable, readable;

    public Access(String path, boolean isFile, boolean writable, boolean readable){
        this.path = path;
        this.isFile = isFile;
        this.writable = writable;
        this.readable = readable;
    }

    public boolean isFile() {
        return isFile;
    }

    public boolean isReadable() {
        return readable;
    }

    public boolean isWritable() {
        return writable;
    }

    public  String getPath() {
        return path;
    }
}
