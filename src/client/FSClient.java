package client;

import javax.net.ssl.*;
import java.io.*;
import java.nio.charset.StandardCharsets;
import java.security.KeyStore;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class FSClient {
    static String host = null;
    static int port = -1;
    static String user = null;
    static String working_directory="/";

    //Starting point
    public static void main(String[] args) throws IOException {

        if (args.length < 2) {
            System.out.println(
                    "USAGE: java FSClient " +
                            "host port ");
            System.exit(-1);
        }

        try {
            host = args[0];
            port = Integer.parseInt(args[1]);
        } catch (IllegalArgumentException e) {
            System.out.println("USAGE: java FSClient " +
                    "host port ");
            System.exit(-1);
        }
        try {

            /*
             * Set up a key manager for client authentication
             * if asked by the server.  Use the implementation's
             * default TrustStore and secureRandom routines.
             */
            System.out.println("Creating socket..");
            SSLSocketFactory factory = null;
            try {
                SSLContext ctx;
                KeyManagerFactory kmf;
                KeyStore ks;
//		char[] passphrase = "passphrase".toCharArray();
                char[] passphrase = "123456".toCharArray();//

                ctx = SSLContext.getInstance("TLS");
                kmf = KeyManagerFactory.getInstance("SunX509");
                ks = KeyStore.getInstance("JKS");

                ks.load(new FileInputStream("APIClientkeystore"), passphrase);

                kmf.init(ks, passphrase);
                ctx.init(kmf.getKeyManagers(), null, null);

                factory = ctx.getSocketFactory();
            } catch (Exception e) {
                throw new IOException(e.getMessage());
            }


            SSLSocket socket= (SSLSocket)factory.createSocket(host,port);
            socket.setUseClientMode(true);

            /*
             * send http request
             *
             * See SSLSocketClient.java for more information about why
             * there is a forced handshake here when using PrintWriters.
             */

            System.out.println("Establishing connection...");
            socket.getSession();
            request_user_login(socket);
            System.out.println("Starting shell...");
            start_shell(socket,factory);
            socket.close();


        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private static void request_user_login(SSLSocket socket) throws IOException, NoSuchAlgorithmException {
        //TODO: CICLO INFITO E CURRENT USER
        boolean allowed = false;
        while(!allowed){
            Console console = System.console();
            BufferedReader input = new BufferedReader(new InputStreamReader(System.in));
            System.out.print("login:");
            String username = console != null ? console.readLine() : input.readLine();
            System.out.print("\npassword:");
            String password = console != null ? console.readPassword().toString() : input.readLine();
            MessageDigest digest = MessageDigest.getInstance("SHA-256");
            byte[] encodedhash = digest.digest(
                    password.getBytes(StandardCharsets.UTF_8));
            String msg = username+" "+bytesToHex(encodedhash);
            PrintWriter  out = new PrintWriter(
                    new BufferedWriter(
                            new OutputStreamWriter(
                                    socket.getOutputStream())));
            out.println("GET login " + msg + "  HTTP/1.1");
            out.println();
            out.flush();

            BufferedReader response = new BufferedReader(
                    new InputStreamReader(
                            socket.getInputStream()));

            String inputLine;
            while ((inputLine = response.readLine()) != null){
                if(inputLine.contains("200 OK")) {
                    allowed = true;
                    user = username;
                }
                else if(!inputLine.contains("200 OK") && !inputLine.contains("Content-Length") && !inputLine.contains("Content-Type")  )
                    if(inputLine.contains("/"))
                        working_directory = inputLine+"/";


            }
            System.out.println("\n200 OK Login Successful!\n");
            response.close();

        }


    }
    private static boolean wants_off(String line){
        return line.equals("quit") || line.equals("logout");
    }

    private static void start_shell(SSLSocket socket, SSLSocketFactory factory) throws Exception {
        socket = (SSLSocket)factory.createSocket(host,port);
        socket.getSession();
        System.out.println("Welcome to Curtes? Remote Filesystem!\n\n");
        BufferedReader input = new BufferedReader(new InputStreamReader(System.in));
        String line;
        System.out.print(user+"$:"+working_directory);
        PrintWriter out;
        while(!wants_off(line = input.readLine())){
            if(line.split(" ")[0].equals("write")){
                System.out.println("\n You may write the file. Write EOF to finish.");
                System.out.println("The file will only be written if it exists and you have access.\n");
                BufferedReader input_reader = new BufferedReader(new InputStreamReader(System.in));
                ByteArrayOutputStream file_writer = new ByteArrayOutputStream();
                while(true) {
                    String line1 = input_reader.readLine();
                    if (line1.equals("EOF")){
                        file_writer.close();
                        break;
                    }
                    file_writer.write((line1+"/n").getBytes());
                }
                 out = new PrintWriter(
                        new BufferedWriter(
                                new OutputStreamWriter(
                                        socket.getOutputStream())));
                out.println("GET "+line+" "+file_writer.toString()+"\nHTTP/1.1");
                out.println();
                out.flush();


                if (out.checkError())
                    System.out.println(
                            "SSLSocketClient: java.io.PrintWriter error");
            }
            else {
               out = new PrintWriter(
                        new BufferedWriter(
                                new OutputStreamWriter(
                                        socket.getOutputStream())));
                out.println("GET " + line + "  HTTP/1.1");
                out.println();
                out.flush();


                /*
                 * Make sure there were no surprises
                 */
                if (out.checkError())
                    System.out.println(
                            "SSLSocketClient: java.io.PrintWriter error");
            }

            /* read response */
            BufferedReader response = new BufferedReader(
                    new InputStreamReader(
                            socket.getInputStream()));

            String inputLine;
            String command[] = line.split(" ");

            while ((inputLine = response.readLine()) != null){

                if(!inputLine.contains("200 OK") && !inputLine.contains("Content-Length") && !inputLine.contains("Content-Type")  )
                    if(command[0].equals("cd") && !inputLine.contains("400") && !inputLine.contains("HTTP/1."))
                        working_directory = inputLine;
                    else System.out.println(inputLine);

            }
            response.close();
            socket = (SSLSocket)factory.createSocket(host,port);
            System.out.print("\n"+user+"$:"+working_directory);
        }
        out = new PrintWriter(new BufferedWriter(
                new OutputStreamWriter(
                        socket.getOutputStream())));
        out.println("GET quit HTTP/1.1");
        out.println();
        out.flush();
        if (out.checkError())
            System.out.println(
                    "SSLSocketClient: java.io.PrintWriter error");
        boolean allowed = false;


        BufferedReader response = new BufferedReader(
                new InputStreamReader(
                        socket.getInputStream()));

        String inputLine;

        while ((inputLine = response.readLine()) != null)
            if(inputLine.contains("/"))
                allowed = true;

        response.close();
        out.close();
        if(!allowed)
            throw new Exception("Something went wrong");

        input.close();

    }
    private static String bytesToHex(byte[] hash) {
        StringBuffer hexString = new StringBuffer();
        for (int i = 0; i < hash.length; i++) {
            String hex = Integer.toHexString(0xff & hash[i]);
            if(hex.length() == 1) hexString.append('0');
            hexString.append(hex);
        }
        return hexString.toString();
    }
}