package authentication;

import access_control.AccessControlServer;
import access_control.AccessFileReader;
import api.ClassServer;
import exceptions.UserNotFoundException;
import exceptions.WrongCredentialsException;
import storage.FileServerStorage;

import javax.net.ServerSocketFactory;
import javax.net.ssl.KeyManagerFactory;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLServerSocket;
import javax.net.ssl.SSLServerSocketFactory;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.ServerSocket;
import java.security.KeyStore;
import java.util.HashMap;
import java.util.Properties;

public class AuthenticationServer extends ClassServer {

    //TODO
    private static int port = -1;
    private static Properties properties = new Properties();
    /**
     * Constructs a ClassServer based on <b>ss</b> and
     * obtains a file's bytecodes using the method <b>getBytes</b>.
     *
     * @param ss
     */
    protected AuthenticationServer(ServerSocket ss) {
        super(ss);
    }

    //Starting point
    public static void main(String[] args) throws IOException {

        if(args.length < 2){
            System.out.println(
                    "USAGE: java [-Djavax.net.ssl.trustStore=FSservertruststore] FileServerStorage port [TLS [true]]");
            System.out.println("");
        }
        else{

            if(args.length >= 1){
                port = Integer.parseInt(args[0]);
            }
            String type = "PlainSocket";
            if(args.length >= 2){
                type=args[1];
            }
            //server
            start_server(type,port,args);
            //se tudo correr bem, load dos users
            InputStream inputStream = new FileInputStream("users.properties");
            if (inputStream == null) {
                System.err.println("Configuration file not found!");
                System.exit(1);
            }

            properties.load(inputStream);



        }
        //System.out.println(check_access_for_command("ls /user1/abcdef/", "user1"));

    }
    //This method will start the server. The socket is created and thrown into the super constructor.
    //The super class extends a Runnable interface so a thread will be created.
    private static void start_server(String type, int port, String[] args) {
        try {
            // Create a Socket Factory of type mentioned
            // Plain Socket ... or SSL Socket and creates
            // the server socket in the specified port
            ServerSocketFactory ssf = getServerSocketFactory(type);
            ServerSocket ss = ssf.createServerSocket(port);
            ((SSLServerSocket) ss).setEnabledProtocols(new String[]{"TLSv1.2"});
            ((SSLServerSocket) ss).setEnabledCipherSuites(new String[]{"TLS_RSA_WITH_AES_128_GCM_SHA256"});
            ((SSLServerSocket) ss).setNeedClientAuth(true);

            new AuthenticationServer(ss);
        } catch (IOException e) {
            System.out.println("Problem with sockets: unable to start ClassServer: " +
                    e.getMessage());
            e.printStackTrace();

        }
    }

    @Override
    public byte[] getBytes(String path) throws Exception {

        //PATH = USER+" "+HASHED PASSWORD
        if(path.equals("shutdown"))
            return "shutdown".getBytes();
        String[] split_path = path.split(" ");
        String user = split_path[0];
        String hashed_pw = split_path[1];
        for(int i=2; i<split_path.length;i++)
            hashed_pw+=" "+split_path[i];
        if(properties.getProperty(user) == null)
            throw new UserNotFoundException("login: user not found");
        if(!properties.getProperty(user).equals(hashed_pw))
            throw new WrongCredentialsException("invalid login");
        return "".getBytes();

    }

    public static ServerSocketFactory getServerSocketFactory(String type) {
        if (type.equals("TLS")) {
            SSLServerSocketFactory ssf = null;
            try {
                // set up key manager to do server authentication
                SSLContext ctx;
                KeyManagerFactory kmf;
                KeyStore ks;

                // Depending on the passphrase used to protect the
                // FSserverkeystore used. Remember, this is the keystore
                // where the server stores the keys.

                //char[] passphrase = "passphrase".toCharArray();
                char[] passphrase = "123456".toCharArray();//

                ctx = SSLContext.getInstance("TLS");
                kmf = KeyManagerFactory.getInstance("SunX509");
                ks = KeyStore.getInstance("JKS");

//              keystore - chaves do servidor: criada com passwd indicada

                ks.load(new FileInputStream("authkeystore"), passphrase);//
                kmf.init(ks, passphrase);
                ctx.init(kmf.getKeyManagers(), null, null);

                // only to deal with the keystore internal representation
                // you have KeyStore class with a lot of methods - see
                // the Java documentation.
                // Ex., KeyStore.getKey() takes a string corresponding to
                // an alias name and a char array representing a password
                // protecting the entry of the keystore, returning
                // java.security.Key object


                ssf = ctx.getServerSocketFactory();
                return ssf;
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            return ServerSocketFactory.getDefault();
        }
        return null;
    }
}
