package storage;

import java.io.Serializable;
import java.time.Instant;


public class FileAttributes implements Serializable {

    private long create_time, mod_time;
    private Instant create_date, mod_date;
    //USEFUL FOR NAMING EXTENSIONS AND OTHER INFO
    private String[] otherspecs;
    private static final long serialVersionUID = 43L;

    public FileAttributes(long create_time, long mod_time, String[] otherspecs){
        this.create_time = create_time;
        this.mod_time = mod_time;
        this.otherspecs=otherspecs;
        this.create_date = Instant.ofEpochMilli(create_time);
        this.mod_date = Instant.ofEpochMilli(mod_time);
    }
    public FileAttributes(long create_time, long mod_time){
        this.create_time = create_time;
        this.mod_time = mod_time;
        this.otherspecs = new String[] { };
        this.create_date = Instant.ofEpochMilli(create_time);
        this.mod_date = Instant.ofEpochMilli(mod_time);

    }

    public String getCreate_date() {
        return create_date.toString();
    }

    public String getMod_date() {
        return mod_date.toString();
    }

    public String[] getOtherspecs() {
        return otherspecs;
    }

    public void setMod_time(long mod_time) {
        this.mod_time = mod_time;
        this.mod_date = Instant.ofEpochMilli(mod_time);
    }
}
