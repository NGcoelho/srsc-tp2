package storage;

import java.io.Serializable;

public abstract class ServerResource implements Resource, Serializable {
    protected String resource_name, full_path;
    protected ServerDirectory parent;
    protected FileAttributes attributes;
    private static final long serialVersionUID = 42L;

    public ServerResource(String resource_name, ServerDirectory parent){
        this.resource_name = resource_name;
        this.parent = parent;
        this.full_path = find_full_path();
        long time = System.currentTimeMillis();
        this.attributes = new FileAttributes(time,time);
    }

    @Override
    public String getResourceName() {
        return resource_name;
    }


    @Override
    public ServerDirectory get_parent() {
        return parent;
    }

    @Override
    public String get_full_path() {
        return full_path;
    }

    @Override
    public String find_full_path() {
        if(resource_name.equals("/"))
            return resource_name;

        StringBuilder builder = new StringBuilder();
        builder.insert(0,"/"+resource_name);
        ServerDirectory current = parent;
        while(current != null){
            if(current.get_parent() != null)
                builder.insert(0,"/"+current.getResourceName());
            current = current.get_parent();
        }
        return builder.toString();
    }

    @Override
    public FileAttributes getAttributes() {
        return attributes;
    }

    @Override
    public void change_mod_date(long mod_time) {
        this.attributes.setMod_time(mod_time);
    }
}
