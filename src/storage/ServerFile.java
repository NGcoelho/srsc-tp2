package storage;

import exceptions.ResourceNotSpecifiedException;

/**Esta classe representa um ficheiro simples, a ser guardado nas diretorias do custom filesytem
 * **/

public class ServerFile extends ServerResource {

    private byte[] content;

    //PARENT DIRECTORY MUST BE SPECIFIED
    public ServerFile (String file_name, ServerDirectory parent) throws ResourceNotSpecifiedException {
        super(file_name,parent);
        if (parent == null)
            throw new ResourceNotSpecifiedException("Parent Directory Not Specified");
        if(file_name == null )
            throw new ResourceNotSpecifiedException("File Name is Undefined");

    }

    public ServerFile(String resourceName, ServerDirectory parent, byte[] contents) throws ResourceNotSpecifiedException {
        super(resourceName,parent);
        if (parent == null)
            throw new ResourceNotSpecifiedException("Parent Directory Not Specified");
        if(resourceName== null )
            throw new ResourceNotSpecifiedException("File Name is Undefined");
        this.write(contents);
    }

    @Override
    public boolean isFileOrDirectory() {
        return true;
    }


    public void rename(String new_name){
        this.resource_name=new_name;
    }
    public byte[] read (){
        return content;
    }
    public void write(byte[] new_content){
        this.content=new_content;
        this.change_mod_date(System.currentTimeMillis());
    }
}
