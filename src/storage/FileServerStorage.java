package storage;





import api.ClassServer;
import exceptions.*;

import javax.net.ServerSocketFactory;
import javax.net.ssl.KeyManagerFactory;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLServerSocket;
import javax.net.ssl.SSLServerSocketFactory;
import java.io.*;
import java.net.ServerSocket;
import java.security.KeyStore;
import java.util.*;

//TODO: verificar se caminho dos pedidos (em get bytes) esta bem formatado
//criar o ciclo para esperar os pedidos;

public class FileServerStorage extends ClassServer {

    private static final String NEWLINE = "\n";
    private static final String ENTER="";
    private static final String EXIT="shutdown";
    private static final String LS="ls";
    private static final String MKDIR="mkdir";
    private static final String CD="cd";
    private static final String RM= "rm";
    private static final String TOUCH="touch";
    private static final String WRITE="write";
    private static final String READ="read";
    private static final String CP="cp";
    private static final String FILE="file";
    private static final String PWD="pwd";
    private static ServerDirectory working_directory,root;
    private static int DefaultServerPort = 2001;


    /**
     * This is the file storage sever.
     * It waits for requests of the API and when received, it processes them accordingly.
     * TODO: A method needs be created to shutdown the file server properly , so it saves to the file.
     *
     * **/





    /**
     * Constructs a ClassServer based on <b>ss</b> and
     * obtains a file's bytecodes using the method <b>getBytes</b>.
     *
     * @param ss
     */
    protected FileServerStorage(ServerSocket ss) throws ResourceNotSpecifiedException {
        super(ss);
        attempt_load_fs();
    }


    //Starting point
    public static void main(String[] args) throws IOException, ResourceNotSpecifiedException {


        if(args.length < 2){
            System.out.println(
                            "USAGE: java [-Djavax.net.ssl.trustStore=FSservertruststore] FileServerStorage port [TLS [true]]");
            System.out.println("");
        }
        else{
            int port = DefaultServerPort;

            if(args.length >= 1){
                port = Integer.parseInt(args[0]);
            }
            String type = "PlainSocket";
            if(args.length >= 2){
                type=args[1];
            }
            //server
            start_server(type,port,args);
        }
    }

    //This method will start the server. The socket is created and thrown into the super constructor.
    //The super class extends a Runnable interface so a thread will be created.
    private static void start_server(String type, int port, String[] args) throws ResourceNotSpecifiedException {
        try{
            // Create a Socket Factory of type mentioned
            // Plain Socket ... or SSL Socket and creates
            // the server socket in the specified port
            ServerSocketFactory ssf = FileServerStorage.getServerSocketFactory(type);
            ServerSocket ss = ssf.createServerSocket(port);
            ((SSLServerSocket)ss).setEnabledProtocols(new String[] { "TLSv1.2"});
            ((SSLServerSocket)ss).setEnabledCipherSuites(new String[] {"TLS_RSA_WITH_AES_128_GCM_SHA256"});
            ((SSLServerSocket)ss).setNeedClientAuth(true);

            new FileServerStorage(ss);
        }
            catch(IOException e){
            System.out.println("Problem with sockets: unable to start ClassServer: " +
                    e.getMessage());
            e.printStackTrace();

        }

    }
    //This method attemps to load the filesystem, or to create one if it fails
    private static void attempt_load_fs() throws ResourceNotSpecifiedException {
        try{
            FileInputStream file_in = new FileInputStream("filesystem.ser");
            ObjectInputStream in = new ObjectInputStream(file_in);
            root = (ServerDirectory) in.readObject();
            working_directory = root;
            in.close();
            file_in.close();
            System.out.println("Saved Data loaded with success!\n");
        }catch (IOException e){
            //THIS IS TO HAPPEN CASE A SAVE FILE IS NOT FOUND
            root = new ServerDirectory("/",null);
            System.out.println("Root Directory Created\n");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }


    /**
     * The FILE command 1.
     * Requires name of  1 or more files to display info
     *
     *
     * @return**/
    private static byte[] file_info(String[] command) throws WrongNumberArgsException, ResourceNotSpecifiedException, ResourceNotExistsException, IOException {
        if(command.length == 1)
            throw new WrongNumberArgsException("file: missing arguments.");
        ByteArrayOutputStream response = new ByteArrayOutputStream();
        for (int i = 1; i < command.length; i++) {
            response.write(print_file_info(command[i]));
        }
        return response.toByteArray();
    }
    /**
     * The FILE command 2.
     *
     *
     * @return**/
    private static byte[] print_file_info(String path) throws ResourceNotSpecifiedException, ResourceNotExistsException, IOException {
        String path_array[] = path.split("/");
        ByteArrayOutputStream response = new ByteArrayOutputStream();
        Resource current;
        if(path.charAt(0) == '/')
            current = retrieveDirectory(path_array,path_array.length,root);
        else current = retrieveDirectory(path_array,path_array.length,working_directory);
        if(current == null)
            throw new ResourceNotExistsException("file: impossible to read info of'"+path+"': Non existing file or directory");
        String type = current instanceof ServerFile ? "file" : "directory";
        response.write(("------Info for "+type+" '"+current.getResourceName()+"'------\n\n\n").getBytes());
        FileAttributes attributes = current.getAttributes();
        response.write(("Creation date: \t"+attributes.getCreate_date()+NEWLINE).getBytes());
        response.write(("Date of last modification: \t"+attributes.getCreate_date()+"\n\n").getBytes());
        return response.toByteArray();
        //PRINTAR OTHERSPECS QUANDO SE USAR

    }
    /**
     * The CP command 1.
     * Requires 1 or more sources and 1 destination
     *
     *
     * @return**/
    private static byte[] copy(String[] command) throws WrongNumberArgsException, WrongResourceTypeException, ResourceNotSpecifiedException, ResourceNotExistsException {
        boolean r_enabled = command.length != 1 && command[1].equals("-r");
        if((command.length < 4 && r_enabled) || (command.length == 1))
            throw new WrongNumberArgsException("cp: missing arguments.");
        // ex: cp file.txt /user/abc/other.txt /user2/destination
        //ex cp -r /user/abc/ /user/def/ /otheruser/ (multiple sources, 1 destination)
        String destination = command[command.length-1];
        for (int i = 1; i < command.length-1 ; i++) {
            if(command[i].equals("-r")) continue;
            copy_from_source_to_dest(command[i],destination, r_enabled);
        }


        return NEWLINE.getBytes();
    }
    /**
     * The CP command 2.
     *
     *
     * @return**/
    private static void copy_from_source_to_dest(String source, String destination,boolean r_enabled) throws WrongResourceTypeException, ResourceNotSpecifiedException, ResourceNotExistsException {
        // cp file -> file2 -> copy file contents (file2 may not exist)
        //cp file -> directory/ -> create new file w same contents in directory

        //CHECK SOURCE : MAY BE FILE OR DIRECTORY
        //TRUE : FILE     FALSE:DIRECTORY
        boolean src_fileORdct = source.charAt(source.length()-1) != '/';
        boolean dest_fileORdct = destination.charAt(destination.length()-1) != '/';

        //HANDLE EXCEPTIONS
        if(!src_fileORdct && !r_enabled)
            throw new WrongResourceTypeException("cp: -r not specified; omitting directory '"+source+ "'");
        if(!src_fileORdct && dest_fileORdct)
            throw new WrongResourceTypeException("cp: impossible to overwrite non-directory '"+destination+ "' with directory '"+source+"' ");

        //RETRIEVE RESOURCES: DESTINATION MIGHT BE NULL
        Resource source_r, destination_r;
        String[] source_path_array = source.split("/");
        if(source.charAt(0) == '/')
            source_r = retrieveDirectory(source_path_array,source_path_array.length,root);
        else source_r = retrieveDirectory(source_path_array,source_path_array.length,working_directory);
        String[] destination_path_array = destination.split("/");
        if(destination.charAt(0) == '/')
            destination_r = retrieveDirectory(destination_path_array,destination_path_array.length,root);
        else destination_r = retrieveDirectory(destination_path_array,destination_path_array.length,working_directory);

        if(source_r == null)
            throw new ResourceNotExistsException("cp: impossible to copy '"+source+ "': File does not exist");

        //DIRECTORY -> DIRECTORY
        if(source_r instanceof ServerDirectory && destination_r instanceof ServerDirectory){
            ((ServerDirectory) destination_r).add_content(new ServerDirectory(source_r.getResourceName(),destination_r.get_parent(),((ServerDirectory) source_r).getDirectory_contents()));
        }
        //DIRECTORY -> DIRECTORY (NOT EXISTING)
        if(source_r instanceof  ServerDirectory && destination_r == null){
            if(destination.charAt(0) == '/')
                destination_r = retrieveDirectory(destination_path_array,destination_path_array.length-1,root);
            else destination_r = retrieveDirectory(destination_path_array,destination_path_array.length-1,working_directory);
            if(destination_r == null)
                throw new ResourceNotExistsException("cp: impossible to copy to '"+destination+ "': File does not exist");
            ((ServerDirectory)destination_r).add_content(new ServerDirectory(destination_path_array[destination_path_array.length-1], (ServerDirectory)destination_r, ((ServerDirectory) source_r).getDirectory_contents()));
        }

        //FILE -> DIRECTORY
        else if(source_r instanceof ServerFile && destination_r instanceof ServerDirectory){
            ((ServerDirectory) destination_r).add_content(new ServerFile(source_r.getResourceName(),destination_r.get_parent(),((ServerFile)source_r).read()));

        }
        //FILE --> FILE
        else if(source_r instanceof ServerFile && destination_r instanceof ServerFile){
            ((ServerFile) destination_r).write(((ServerFile) source_r).read());
            ServerDirectory parent = destination_r.get_parent();
            parent.replace_file((ServerFile)destination_r);
        }
        //FILE --> FILE (NOT EXISTING)
        else if(source_r instanceof ServerFile && destination_r == null){
            if(!dest_fileORdct)
                throw new ResourceNotExistsException("cp: impossible to copy to '"+destination+ "': Directory does not exist");
            if(destination.charAt(0) == '/')
                destination_r = retrieveDirectory(destination_path_array,destination_path_array.length-1,root);
            else destination_r = retrieveDirectory(destination_path_array,destination_path_array.length-1,working_directory);
            if(destination_r == null)
                throw new ResourceNotExistsException("cp: impossible to copy to '"+destination+ "': File does not exist");
            ((ServerDirectory)destination_r).add_content(new ServerFile(destination_path_array[destination_path_array.length-1], (ServerDirectory)destination_r, ((ServerFile) source_r).read()));
        }





    }
    /**
     * The RM command 1. Requires 1 or more paths.
     *
     * @return**/
    private static byte[] remove(String[] command) throws WrongNumberArgsException, ResourceNotSpecifiedException, WrongResourceTypeException, ResourceNotExistsException {
        boolean r_enabled = command.length != 1 && command[1].equals("-r");
        if((command.length < 3 && r_enabled) || (command.length == 1))
            throw new WrongNumberArgsException("rm: missing arguments.");
        //rm file.txt   rm /user/file.txt  anotherfile.txt
        //rm -r directory  /user/abc

        for (int i = r_enabled ? 2 : 1; i < command.length; i++) {

            remove_resource(command[i],r_enabled);
        }
        return NEWLINE.getBytes();
    }
    /**
     * The RM command 2.
     *
     * @return**/
    private static void remove_resource(String path, boolean r_enabled) throws ResourceNotSpecifiedException, WrongResourceTypeException, ResourceNotExistsException {
        String path_array[] = path.split("/");
        Resource current;
        if(path.charAt(0) == '/')
            current = retrieveDirectory(path_array,path_array.length,root);
        else current = retrieveDirectory(path_array,path_array.length,working_directory);
        if(current == null)
            throw new ResourceNotExistsException("rm: impossible to remove «"+path+ "» : File does not exist.");
        if(current instanceof ServerDirectory && !r_enabled)
            throw new WrongResourceTypeException("rm: impossible to remove «"+path+ "» : It's a directory");
        ServerDirectory parent = current.get_parent();
        if(parent == null)
            throw new WrongResourceTypeException("rm : root directory is not removable.");
        parent.remove_resource(current.getResourceName());
    }
    /**
     * The READ command . Requires name of file to be read
     *
     * @return**/
    private static byte[] read_file(String[] command) throws WrongNumberArgsException, ResourceNotSpecifiedException, WrongResourceTypeException {
        //read path_to_file
        //read file.txt
        //
        if(command.length > 2)
            throw new WrongNumberArgsException("read: too much arguments");
        if(command.length < 2)
            throw new WrongNumberArgsException("read: missing arguments");
        String path_array[] = command[1].split("/");
        Resource current;
        if(command[1].charAt(0) == '/')
            current = retrieveDirectory(path_array,path_array.length,root);
        else current = retrieveDirectory(path_array,path_array.length,working_directory);
        if(current instanceof ServerDirectory)
            throw new WrongResourceTypeException("read: impossible to read «"+command[1]+ "» : Not a file.");
        ServerFile file = (ServerFile) current;
        return file.read();
    }

    /**
     * The WRITE command . Requires name of file to be written and the content to write
     * ( in the old version a writing prompt would appear, that is on the client side now)
     *
     * @return**/

    private static byte[] write_file(String[] command) throws WrongNumberArgsException, ResourceNotSpecifiedException, WrongResourceTypeException, IOException {

        //write path_to_file --> this will prompt something to ask to write
        //ex: write file.txt
        //ex: write path/to/file.txt


        if(command.length < 2)
            throw new WrongNumberArgsException("write: missing arguments");
        String to_write = "";
        for(int i = 2; i< command.length;i++){
            to_write += command[i]+" ";
        }
        String path_array[] = command[1].split("/");
        Resource current;
        if(command[1].charAt(0) == '/')
            current = retrieveDirectory(path_array,path_array.length,root);
        else current = retrieveDirectory(path_array,path_array.length,working_directory);

        if(current instanceof ServerDirectory)
            throw new WrongResourceTypeException("write: impossible to write «"+command[1]+ "» : Not a file.");
        ServerFile file = (ServerFile) current;


        //WE HAVE THE CONTENS, NOW WE HAVE TO UPDATE THE FILE
        //GET FILE'S PARENT , AND FROM THE PARENT REPLACE THAT FILE
        ServerDirectory parent = file.get_parent();
        //GET THEM EOL STRAIGHT

        file.write(to_write.replaceAll("/n","\n").getBytes());
        parent.replace_file(file);
        System.out.println();
        return new byte[0];
    }

    /**
     * The touch command 1. arguments are  names for new files. If name is not a full path, file to be created in working directory
     * Otherwise file is created in specified path
     *
     * @return**/
    private static byte[] create_file(String[] command) throws ResourceNotSpecifiedException, ResourceAlreadyExistsException, ResourceNotExistsException, WrongResourceTypeException {
        //example1: touch teste.txt ---> working directory
        //example2: touch /user/teste1/ye.txt --> retrieve teste1 directory
        //example 3 touch teste.txt /user/teste1/ye.txt
        if(command.length == 1)
            throw new ResourceNotSpecifiedException("touch: file(s) name(s) not specified");

        for (int i = 1; i < command.length ; i++) {
            create_file_in_directory(command[i]);
        }

        return NEWLINE.getBytes();
    }

    /**
     * The TOUCH command 2. If typed w/ no args, complains.
     * Requires the name of file to be created
     *
             *
             * @return**/

    private static void create_file_in_directory(String path) throws ResourceNotSpecifiedException, ResourceAlreadyExistsException, WrongResourceTypeException, ResourceNotExistsException {
        String[] path_array = path.split("/");
        ServerDirectory directory;
        //working dc
        if(path_array.length == 1){
            directory = working_directory;
            //working_directory.add_content(new ServerFile(path, working_directory));
        }
        else{
            Resource current;
            if(path.charAt(0) == '/')
                current = retrieveDirectory(path_array,path_array.length-1,root);
            else current = retrieveDirectory(path_array,path_array.length-1,working_directory);
            if(current instanceof ServerFile)
                throw new WrongResourceTypeException("touch: impossible to create file «"+path+ "» : Not a directory.");
            if(current == null)
                throw new ResourceNotExistsException("touch: impossible to create file «"+path+ "» : Non existing file or directory.");
            directory = (ServerDirectory)current;
        }

        if(directory.check_content_exists(path))
            throw new ResourceAlreadyExistsException("touch: impossible to create file «"+path+ "» : File already exists.");
        directory.add_content(new ServerFile(path_array[path_array.length-1], directory));

    }

    /**
     * The CD command. If typed w/ no args, changes the current directory to root.
     * If typed with a directory path, changes the current directory to the specified
     *
     *
     * @return**/
    private static byte[] change_directory(String[] command) throws ResourceNotSpecifiedException, ResourceNotExistsException, ResourceAlreadyExistsException, WrongNumberArgsException, WrongResourceTypeException {

        ServerDirectory previous_directory = working_directory; //In case things go wrong;

        if(command.length==1)//case no args: just change to the root directory
            working_directory = (ServerDirectory)retrieveDirectory(new String[]{"/"},0,root);

        else if(command.length==2){//case 1 args: retrieve and change to the specified directory
            if(command[1].equals("..") && working_directory != root)
                working_directory = working_directory.get_parent();
            else if(!command[1].equals("..")){
                String[] path_array = command[1].split("/");
                Resource resource = retrieveDirectory(path_array, path_array.length,working_directory);
                if(resource instanceof ServerFile)
                    throw new WrongResourceTypeException("cd: «"+command[1]+"»: Not a directory");
                working_directory = (ServerDirectory)resource;
                if(working_directory == null) {
                    working_directory = previous_directory;
                    throw new ResourceNotExistsException("impossible to access «"+command[1]+"» : File does not exist.");
                }
            }
        }
        else throw new WrongNumberArgsException("cd: too much arguments.");

        return working_directory.get_full_path().getBytes();
    }

    /**
     * The MKDIR command 1. If typed w/ no args, throws an error. If typed with 1 or more paths it will create the new directories in those paths,if they exist.
     *
     *
     * @return**/

    private static byte[] make_directory(String[] command) throws ResourceAlreadyExistsException, ResourceNotSpecifiedException, ResourceNotExistsException, WrongResourceTypeException {
        //mkdir user path --> USER AQUI NAO INTERESSA, ISSO É RESPONSIBILIDADE DO AUTH
        if(command.length==1)
            throw new ResourceNotSpecifiedException("mkdir: directory name(s) is(are) missing.");

        for (int i = 1; i < command.length ; i++) {
            make_directory_in_path(command[i]);
        }
        return NEWLINE.getBytes();
    }

    /**
     * The MKDIR command 2.
     *
     *
     * @return**/
    private static void make_directory_in_path(String path) throws ResourceAlreadyExistsException, ResourceNotSpecifiedException, WrongResourceTypeException, ResourceNotExistsException {
        //CASO mkdir new_dir --> criar na working directory SE nome não existir
        //ex: mkdir /nunoc/foo/bar
        if(!path.contains("/")){
            //working directory
            if(working_directory.check_content_exists(path))
                throw new ResourceAlreadyExistsException("mkdir: impossible to create directory «"+path+ "» : File already exists.");
            working_directory.add_content(new ServerDirectory(path,working_directory));
        }
        else{
            String path_array[] = path.split("/");
            Resource current;
            if(path.charAt(0) == '/')
                current = retrieveDirectory(path_array,path_array.length-1,root);
            else current = retrieveDirectory(path_array,path_array.length-1,working_directory);
            if(current instanceof ServerFile)
                throw new WrongResourceTypeException("mkdir: impossible to create directory «"+path+ "» : Not a directory.");
            ServerDirectory directory = (ServerDirectory)current;
            if(directory == null)
                throw new ResourceNotExistsException("mkdir: impossible to create directory «"+path+ "» : Directory does not exist.");
            if(directory.check_content_exists(path_array[path_array.length-1]))
                throw new ResourceAlreadyExistsException("mkdir: impossible to create directory «"+path+ "» : File already exists.");
            directory.add_content(new ServerDirectory(path_array[path_array.length-1],directory));
        }
    }

    /**
     * The LS command. If typed w/ no arg, just prints the contents of the working directory.
     * If typed w/ 1 or + paths, it will print the contents of each of the directories
     * TODO: ls throws unhandled error if a file is specified. In Unix when ls is used w/ file, it just prints file name. mb Copy?
    * **/
    private static byte[] list_directory_contents(String[] command) throws ResourceNotSpecifiedException, ResourceNotExistsException, ResourceAlreadyExistsException, IOException {
        ByteArrayOutputStream response = new ByteArrayOutputStream();
        if(command.length == 1){
            //ls & no arguments: just print the contents of the working directory
            for(String name: working_directory.getDirectory_contents().keySet()){
                response.write((name+"\n").getBytes());
            }
        }
        else {
            List<Resource> listing_directories = new LinkedList<>();
            for (int i = 1; i < command.length; i++) {
                String[] patharray = command[i].split("/");

                Resource resource = retrieveDirectory(patharray,patharray.length,working_directory);
                if(resource==null)
                    throw new ResourceNotExistsException("impossible to access «"+command[i]+"» : File does not exist.");
                listing_directories.add(resource);
            }
            for(Resource resource : listing_directories){
                if(resource instanceof ServerDirectory){
                    response.write(("\nContents of "+resource.getResourceName()+": \n").getBytes());

                    for(String resource2 : ((ServerDirectory)resource).getDirectory_contents().keySet()){
                        response.write((resource2+"\n").getBytes());
                    }
                }
                else response.write(("file: "+ resource.get_full_path()+"\n").getBytes());
                response.write(NEWLINE.getBytes());
            }
        }
       return response.toByteArray();
    }
    //Method responsible for doing any persistency work before shutting down
    private static void save() throws IOException {
        //FALTA SERIALZING STUFF
        FileOutputStream out = new FileOutputStream("filesystem.ser");
        ObjectOutputStream obj_out = new ObjectOutputStream(out);
        obj_out.writeObject(root);
        obj_out.close();
        out.close();
        System.out.println("\nData saved in filesystem.ser");

    }
    //Auxiliary method to navigate the file system tree and retrieve a directory in it
    private static Resource retrieveDirectory(String[] patharray,int length,ServerDirectory start) throws ResourceNotSpecifiedException {

        if(patharray.equals(new String[]{"/"}))
            return root;
        Resource current = start;
        //parent quest
        for (int i = 0; i < length; i++) {
            if(patharray[i].equals(""))
                continue;
            if(!((ServerDirectory)current).check_content_exists(patharray[i]))
                return null;
            current =  ((ServerDirectory)current).getDirectory_contents().get(patharray[i]);

        }
        return current;

    }

    /**
     * The getBytes() method is abstract and inherited from Class Server. It is the method that is executed upon the server receiving content.
     * In this implementation, the received path holds the command issue and the path or paths and processes the content accordingly to the issued command.
     * **/

    @Override
    public byte[] getBytes(String path) throws Exception {
        String command[] = path.trim().split(" ");

            switch (command[0]) {
                case ENTER:
                    return NEWLINE.getBytes();
                case EXIT:
                    save();
                   return "shutdown".getBytes();
                case LS:
                    return list_directory_contents(command);
                case MKDIR:
                    return make_directory(command);
                case CD:
                    return change_directory(command);

                case TOUCH:
                    return create_file(command);

                case WRITE://TODO
                    return write_file(command);

                case READ:
                    return read_file(command);

                case RM:
                    return remove(command);
                case CP:
                    return copy(command);

                case FILE://TODO
                    return file_info(command);
                case PWD:
                    return print_working_directory(command);

                default:
                    throw new Exception(command[0]+": command not found.");
            }

    }

    /**
     * The PWD command. Requires no args, and just retrieves the full name of the working directory.
     *
     * **/
    private byte[] print_working_directory(String[] command) {
        return working_directory.get_full_path().getBytes();
    }




    //Auxiliary method used to provide the factory in charge of creating the socket.
    public static ServerSocketFactory getServerSocketFactory(String type) {
        if (type.equals("TLS")) {
            SSLServerSocketFactory ssf = null;
            try {
                // set up key manager to do server authentication
                SSLContext ctx;
                KeyManagerFactory kmf;
                KeyStore ks;

                // Depending on the passphrase used to protect the
                // FSserverkeystore used. Remember, this is the keystore
                // where the server stores the keys.

                //char[] passphrase = "passphrase".toCharArray();
                char[] passphrase = "myserver".toCharArray();//

                ctx = SSLContext.getInstance("TLS");
                kmf = KeyManagerFactory.getInstance("SunX509");
                ks = KeyStore.getInstance("JKS");

//              keystore - chaves do servidor: criada com passwd indicada

                ks.load(new FileInputStream("FSserverkeystore"), passphrase);//
                kmf.init(ks, passphrase);
                ctx.init(kmf.getKeyManagers(), null, null);

                // only to deal with the keystore internal representation
                // you have KeyStore class with a lot of methods - see
                // the Java documentation.
                // Ex., KeyStore.getKey() takes a string corresponding to
                // an alias name and a char array representing a password
                // protecting the entry of the keystore, returning
                // java.security.Key object


                ssf = ctx.getServerSocketFactory();
                return ssf;
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            return ServerSocketFactory.getDefault();
        }
        return null;
    }
}
