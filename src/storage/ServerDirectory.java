package storage;

import exceptions.ResourceNotSpecifiedException;

import java.util.HashMap;

/**
 * Esta classe representa uma directoria do custom file system.
 * Cada diretoria tem nome, uma diretoria parente (exceto root) ,0 ou mais diretorias e 0 ou mais ficheiros
 * **/

//KEEP IT SIMPLE : MKDIR por ex: um metodo para verificar se pasta existe, outro para cria la se nao existir.
    //NAO COMPLIQUEMOS

    //SECALHAR NÃO É MÁ IDEIA GUARDAR OS CAMINHOS ABSOLUTOS(DESDE A ROOT) numa variavel. Isto é fazivel chamando o pai de cada diretoria até chegar a root(pai=null)

public class ServerDirectory extends ServerResource {

    private HashMap<String, Resource> directory_contents;


    //PARENT MAY BE NULL IF DIRECTORY IS ROOT ("/")
    public ServerDirectory(String directory_name, ServerDirectory parent) throws ResourceNotSpecifiedException {
        super(directory_name,parent);
        if(parent == null && !directory_name.equals("/"))
            throw new ResourceNotSpecifiedException("Parent Directory Not Specified and Current Directory is not root");
        this.directory_contents = new HashMap<>();
    }
    public ServerDirectory(String directory_name, ServerDirectory parent,HashMap<String,Resource> contents) throws ResourceNotSpecifiedException {
        super(directory_name,parent);
        if(parent == null && !directory_name.equals("/"))
            throw new ResourceNotSpecifiedException("Parent Directory Not Specified and Current Directory is not root");
        this.directory_contents = contents;
    }


    @Override
    public boolean isFileOrDirectory() {
        return false;
    }


    public HashMap<String, Resource> getDirectory_contents() {
        return directory_contents;
    }
    //ADDING CONTENT TO DIRECTORY: MAY BE A DIRECTORY OR A FILE
    //REQUIRES VERIFICATION THROUGH ANOTHER METHOD
    public void add_content(Resource resource){
        directory_contents.putIfAbsent(resource.getResourceName(),resource);
        this.change_mod_date(System.currentTimeMillis());
    }

    public void replace_file(ServerFile file){
        directory_contents.replace(file.getResourceName(),file);
    }


    public boolean check_content_exists(String resource_name){
        return directory_contents.containsKey(resource_name);
    }

    public void remove_resource(String resource_name){
        directory_contents.remove(resource_name);
        this.change_mod_date(System.currentTimeMillis());
    }

}
