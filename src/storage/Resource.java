package storage;

public interface Resource {

    //falta data de criação e de ultima modificação

     String getResourceName();

     ServerDirectory get_parent();

    boolean isFileOrDirectory();

     String get_full_path();

     String find_full_path();

     FileAttributes getAttributes();

     void change_mod_date(long mod_time);


}
