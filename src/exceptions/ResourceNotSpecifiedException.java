package exceptions;

public class ResourceNotSpecifiedException extends Exception {
    public ResourceNotSpecifiedException(String message) {
        super(message);
    }
}
