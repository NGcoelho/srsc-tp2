package exceptions;

public class UserLoggedInException extends Exception {
    public UserLoggedInException(String s) {
        super(s);
    }
}
