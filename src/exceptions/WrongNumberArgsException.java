package exceptions;

public class WrongNumberArgsException extends Exception {
    public WrongNumberArgsException(String s) {
        super(s);
    }
}
