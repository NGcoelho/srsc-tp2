package exceptions;

public class WrongResourceTypeException extends Exception {
    public WrongResourceTypeException(String s) {
        super(s);
    }
}
