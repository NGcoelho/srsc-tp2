package exceptions;

public class ResourceNotExistsException extends Exception {
    public ResourceNotExistsException(String s) {
        super(s);
    }
}
